# About #  

This repository contains scripts and ansible playbook for configuring AWS EC2 instance.

## How to start ##

For this project I choose AWS Debian EC2 instance. You can find [here](https://docs.aws.amazon.com/efs/latest/ug/gs-step-one-create-ec2-resources.html) how to deploy it. 
Once you have deployed EC2 instance, you can initiate remote configuring it with Ansible.  In case you need to install Ansible on your workstation, you can refer to this [link](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).  
Before start **don't forget** to add IP or public DNS name of your instance to the ./inventory file.  
Then run the command: 

*ansible-playbook -i ./inventory --private-key ./%YOUR_PRIVATE_KEY%.pem  -u admin ./deploy.yml*  

You need to provide path to your private key instead of %YOUR_PRIVATE_KEY%.pem. How to create it described [here](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/get-set-up-for-amazon-ec2.html)  

Once Ansible playbook running is finished you should have Docker, Docker-Compose and GitLab Runner engine installed on your EC2 instance.  
Then you need to register 2 GitLab runners on your remote VM. For this task I created 2 runners: andersen-runner (shell executor) and andersen-docker (docker executor)  
[Here](https://docs.gitlab.com/runner/register/) you can find more details about GitLab runner registration.
